## Descrição

API de clientes escrito em Java com Spring Boot, Spring Data, banco de dados MySQL e Testes com JUnit 5. Fornece serviços CRUD (Create, Read, Update e Delete) para o cadastro de clientes.

## Pre-requisitos
* Git
* Docker
* Java JDK 8+

## Baixar os arquivos
```sh
git clone https://bitbucket.org/frank_santos/clientes-api.git
cd clientes-api
```

## Iniciar o banco de dados

```sh
docker-compose up -d
```

OU via docker manual:

```sh
docker run --name=mysql_clientes \
  -e MYSQL_DATABASE=db_clientes \
  -e MYSQL_USER=usr_clientes \
  -e MYSQL_PASSWORD=pwd_clientes \
  -v "$PWD/mysql_data"://var/lib/mysql \
  -p '3306:3306' \
  -d mysql/mysql-server:8.0
```

## Iniciar a aplicação

Linux ou Mac

```sh
./mvn spring-boot:run
```

OU Windows 

```cmd
mvn.cmd spring-boot:run
```

## Documentação da API

Endpoints:

| Metodo /recurso                | Descrição                                |
|--------------------------------|------------------------------------------|
| POST /clientes                 | Cria um novo cliente                     |
| GET /clientes[?page,size,sort] | Consulta todos os clientes cadastrados   |
| GET /clientes?cpf={cpf}        | Consulta os clientes por CPF             |
| GET /clientes?nome={nome}      | Localiza os clientes por nome            |
| GET /clientes/{id}             | Busca um cliente pelo ID                 |
| PUT /clientes/{id}             | Atualiza todos os dados do cliente       |
| PATCH /clientes/{id}           | Atualiza somente os campos especificados |
| DELETE /clientes/{id}          | Remove um cliente pelo ID                |


### Documentação Swagger ###

 http://localhost:8080/swagger-ui/
