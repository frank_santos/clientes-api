package dev.builders.clientes.controllers;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import dev.builders.clientes.dtos.ClienteDTO;
import dev.builders.clientes.dtos.PageDTO;
import dev.builders.clientes.dtos.ResponseDTO;
import dev.builders.clientes.entities.Cliente;
import dev.builders.clientes.exceptions.NotFoundException;
import dev.builders.clientes.repositories.ClienteRepository;
import dev.builders.clientes.search.ClienteSpecification;
import dev.builders.clientes.services.ClienteService;
import dev.builders.clientes.utils.SortConverter;
import dev.builders.clientes.utils.Views;

@RestController
@RequestMapping(path = "/clientes", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClienteController {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private ClienteService clienteService; 
	
	@PostMapping
	public ResponseEntity<ResponseDTO> create(
		@Valid @RequestBody @JsonView(Views.Create.class) 
		ClienteDTO dados
	) {
		Cliente cliente = dados.toEntity();
		
		clienteService.registrar(cliente);
		
		ResponseDTO response = new ResponseDTO(
			"Cliente registrado com sucesso", 
			Collections.singletonMap("generatedId", cliente.getId())
		);
		
		return ResponseEntity.created(URI.create("/clientes/" + cliente.getId()))
			.body(response);
	}
	
	@GetMapping
	public PageDTO<ClienteDTO> getAll(
		@RequestParam(required = false) String cpf, 
		@RequestParam(required = false) String nome, 
		@RequestParam(required = false, defaultValue = "0") Integer pageNumber, 
		@RequestParam(required = false, defaultValue = "20") Integer pageSize, 
		@RequestParam(required = false, defaultValue = "id") String sort
	) {
		
		ClienteSpecification clienteSpec = new ClienteSpecification()
			.withCpf(cpf)
			.withNome(nome);
		
		PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, SortConverter.toSort(sort));
		
		Page<Cliente> page = clienteRepository.findAll(clienteSpec, pageRequest);
		
		PageDTO<ClienteDTO> pageDTO = PageDTO.of(page.map(ClienteDTO::of));
		
		return pageDTO;
	}
	
	@GetMapping(path = "/{id}")
	public ClienteDTO getOne(@PathVariable Long id) {
		ClienteDTO dto = clienteRepository.findById(id)
			.map(ClienteDTO::of)
			.orElseThrow(() -> new NotFoundException("Cliente não encontrado com o id: " + id));
		
		return dto;
	}
	
	@PutMapping(path = "/{id}")
	public ResponseDTO put(@PathVariable Long id, @RequestBody @JsonView(Views.Update.class) @Valid ClienteDTO dados) {
		clienteRepository.findById(id)
			.orElseThrow(() -> new NotFoundException("Cliente não encontrado com o id: " + id));
		
		Cliente cliente = dados.toEntity();
		cliente.setId(id);
		
		clienteService.atualizar(cliente);
		
		return new ResponseDTO("Registro alterado com sucesso");
	}
	
	@PatchMapping(path = "/{id}")
	public ResponseDTO patch(@PathVariable Long id, @JsonView(Views.Update.class) @RequestBody ClienteDTO dados) {
		Cliente cliente = clienteRepository.findById(id)
			.orElseThrow(() -> new NotFoundException("Cliente não encontrado com o id: " + id));
		
		if (dados.nome != null) {
			cliente.setNome(dados.nome);
		}
		if (dados.cpf != null) {
			cliente.setCpf(dados.cpf);
		}
		if (dados.dataNascimento != null) {
			cliente.setDataNascimento(dados.dataNascimento);
		}
		
		clienteService.atualizar(cliente);
		
		return new ResponseDTO("Dados atualizados com sucesso");
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseDTO delete(@PathVariable Long id) {
		if (!clienteRepository.existsById(id)) {
			throw new NotFoundException("Cliente não encontrado com id " + id);
		}
		
		clienteService.remover(id);
		
		return new ResponseDTO("Registro removido com sucesso");
	}
	
}
