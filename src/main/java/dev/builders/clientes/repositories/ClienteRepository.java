package dev.builders.clientes.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import dev.builders.clientes.entities.Cliente;

@Repository
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long>, JpaSpecificationExecutor<Cliente> {

	boolean existsByCpf(String cpf);

	Cliente findFirstByCpf(String cpf);
}
