package dev.builders.clientes.utils;

public interface Views {
	
	public interface Create {}
	
	public interface Read {}
	
	public interface Update {}
	
}
