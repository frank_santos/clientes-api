package dev.builders.clientes.utils;

import java.util.List;

import br.com.caelum.stella.ValidationMessage;
import br.com.caelum.stella.validation.CPFValidator;

public class ValidationUtils {
	
	private static CPFValidator cpfValidator = new CPFValidator();
	
	public static boolean isValidCPF(String cpf) {
		List<ValidationMessage> errors = cpfValidator.invalidMessagesFor(cpf);

		return errors.isEmpty();
	}

}
