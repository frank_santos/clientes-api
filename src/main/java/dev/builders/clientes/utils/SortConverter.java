package dev.builders.clientes.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.StringUtils;

public class SortConverter {
	
	/**
	 * Converte uma string nos formatos "{campo} [ASC|DESC]" ou "{campo}:[ASC|DESC]" para o objeto Sort
	 * Exemplo: nome ASC, id:ASC
	 */
	public static Sort toSort(String string) {
		if (string == null || string.trim().isEmpty()) {
			return Sort.unsorted();
		}
		List<Order> orders = new ArrayList<>();
		String[] items = string.split(",");
		for (String item : items) {
			if (StringUtils.hasText(item)) {
				orders.add(toOrder(item.trim()));
			}
		}
		return Sort.by(orders);
	}
	
	public static Order toOrder(String string) {
		String[] parts = string.split("[\\s:]");
		if (parts.length > 1 && parts[1].equalsIgnoreCase("desc")) {
			return Order.desc(parts[0]);			
		} else {
			return Order.asc(parts[0]);
		}
	}
	
}
