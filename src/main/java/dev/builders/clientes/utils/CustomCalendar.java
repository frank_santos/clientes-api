package dev.builders.clientes.utils;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 * Classe necessária para poder realizar testes unitarios com regras de negócios que dependem do dia atual.
 * Exemplo: Idade do cliente
 * 
 * @author frank
 *
 */
public class CustomCalendar {
	
	private static Clock clock;
	
	static {
		useSystemDate();
	}
	
	public static LocalDate currentDate() {
		return LocalDate.now(clock);
	}
	
	public static void setCurrentDate(LocalDate date) {
		setCurrentDate(date, ZoneId.systemDefault());
	}
	
	public static void setCurrentDate(LocalDate date, ZoneId zoneId) {
		Clock fixedClock = Clock.fixed(date.atStartOfDay(zoneId).toInstant(), zoneId);
		clock = fixedClock;
	}
	
	public static void useSystemDate() {
		clock = Clock.systemDefaultZone();
	}
	
}
