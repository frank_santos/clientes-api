package dev.builders.clientes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import dev.builders.clientes.entities.Cliente;
import dev.builders.clientes.exceptions.BadRequestException;
import dev.builders.clientes.exceptions.ConflictException;
import dev.builders.clientes.exceptions.ValidationException;
import dev.builders.clientes.repositories.ClienteRepository;
import dev.builders.clientes.utils.CustomCalendar;
import dev.builders.clientes.utils.ValidationUtils;

@Service
public class ClienteService {
	
	@Autowired 
	private ClienteRepository repository;
	
	public Cliente registrar(Cliente cliente) {
		if (cliente.getId() != null) {
			throw new BadRequestException("ID do cliente não pode ser informado. Pois será gerado pelo sistema.");			
		}
		
		validar(cliente);
		
		if (repository.existsByCpf(cliente.getCpf())) {
			throw new ConflictException("CPF já registrado: " + cliente.getCpf());
		}
		
		repository.save(cliente);
		
		return cliente;
	}
	
	public Cliente atualizar(Cliente cliente) {
		validar(cliente);
		
		Cliente clienteComCpfExistente = repository.findFirstByCpf(cliente.getCpf());
		
		if (clienteComCpfExistente != null && !clienteComCpfExistente.equals(cliente)) {
			throw new ConflictException("CPF " + clienteComCpfExistente.getCpf() + " já usado para outro cliente: " + clienteComCpfExistente.getId());
		}
		
		repository.save(cliente);
		
		return cliente;
	}
	
	public void validar(Cliente cliente) {
		if (cliente == null) {
			throw new ValidationException("Dados do cliente são obrigatórios");
		}
		if (!StringUtils.hasText(cliente.getNome())) {
			throw new ValidationException("Nome é obrigatório");
		}
		if (!StringUtils.hasText(cliente.getCpf())) {
			throw new ValidationException("CPF é obrigatório");
		}
		if (!cliente.getCpf().matches("\\d{11}")) {
			throw new ValidationException("CPF deve possuir 11 digitos numéricos");
		}
		if (!ValidationUtils.isValidCPF(cliente.getCpf())) {
			throw new ValidationException("CPF inválido");
		}
		if (cliente.getDataNascimento() == null) {
			throw new ValidationException("Data de Nascimento é obrigatória");
		}
		if (!cliente.getDataNascimento().isBefore(CustomCalendar.currentDate())) {
			throw new ValidationException("Data de Nascimento inválida");
		}
	}

	public void remover(Long id) {
		repository.deleteById(id);
	}
	
}
