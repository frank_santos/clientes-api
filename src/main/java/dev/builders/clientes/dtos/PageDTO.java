package dev.builders.clientes.dtos;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

public class PageDTO<T> {
	
	public List<T> content;
	
	public int contentSize;
	public int pageNumber;
	public int pageSize;
	public long total;
	public String sort;
	
	public static <T> PageDTO<T> of(Page<T> page) {
		PageDTO<T> dto = new PageDTO<>();
		
		dto.content = page.getContent();
		dto.contentSize = dto.content.size();
		dto.pageNumber = page.getNumber();
		dto.pageSize = page.getSize();
		dto.total = page.getTotalElements();
		dto.sort = page.getSort().stream()
			.map(it -> it.getProperty() + " " + it.getDirection())
			.collect(Collectors.joining(", "));
		
		return dto;
	}
	
}
