package dev.builders.clientes.dtos;

import java.util.Map;

public class ResponseDTO {
	
	private String message;
	private Map<String, Object> additionalInfo;
	
	public ResponseDTO(String message) {
		this(message, null);
	}
	
	public ResponseDTO(String message, Map<String, Object> aditionalInfo) {
		this.message = message;
		this.additionalInfo = aditionalInfo;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Map<String, Object> getAdditionalInfo() {
		return additionalInfo;
	}
	
}
