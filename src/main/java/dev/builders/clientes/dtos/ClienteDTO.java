package dev.builders.clientes.dtos;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;

import dev.builders.clientes.entities.Cliente;
import dev.builders.clientes.utils.Views;

public class ClienteDTO {
	
	@JsonView(Views.Read.class)
	public Long id;
	
	@NotBlank @Size(max = 70)
	public String nome;
	
	@NotBlank @Size(min = 11, max = 11)
	public String cpf;
	
	@NotNull @Past
	public LocalDate dataNascimento;
	
	@JsonView(Views.Read.class)
	public Integer idade;
	
	public static ClienteDTO of(Cliente cliente) {
		ClienteDTO dto = new ClienteDTO();
		
		dto.id = cliente.getId();
		dto.nome = cliente.getNome();
		dto.cpf = cliente.getCpf();
		dto.dataNascimento = cliente.getDataNascimento();
		dto.idade = cliente.getIdade();
		
		return dto;
	}
	
	public Cliente toEntity() {
		return toEntity(new Cliente());
	}
	
	public Cliente toEntity(Cliente cliente) {
		cliente.setId(id);
		cliente.setNome(nome);
		cliente.setCpf(cpf);
		cliente.setDataNascimento(dataNascimento);
		
		return cliente;
	}
	
}
