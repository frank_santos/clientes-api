package dev.builders.clientes.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import dev.builders.clientes.entities.Cliente;

public class ClienteSpecification implements Specification<Cliente> {
	
	private static final long serialVersionUID = 1L;
	
	private String cpf;
	private String nome;
	
	public ClienteSpecification withCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}
	
	public ClienteSpecification withNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	@Override
	public Predicate toPredicate(Root<Cliente> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (StringUtils.hasText(cpf)) {
			predicates.add(criteriaBuilder.equal(root.get("cpf"), cpf));
		}
		if (StringUtils.hasText(nome)) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("nome")), "%" + nome.toUpperCase() + "%"));
		}
		
		return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
	}

}
