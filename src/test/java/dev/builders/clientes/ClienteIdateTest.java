package dev.builders.clientes;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import dev.builders.clientes.entities.Cliente;
import dev.builders.clientes.utils.CustomCalendar;

public class ClienteIdateTest {
	
	@Test
	public void clienteCom35Anos() {
		CustomCalendar.setCurrentDate(LocalDate.parse("2020-10-05"));
		
		Cliente cliente = new Cliente();
		
		cliente.setDataNascimento(LocalDate.parse("1985-10-01"));
		
		assertEquals(35, cliente.getIdade());
	}
	
	@Test
	public void clienteCom21Anos() {
		CustomCalendar.setCurrentDate(LocalDate.parse("2021-06-30"));
		
		Cliente cliente = new Cliente();
		
		cliente.setDataNascimento(LocalDate.parse("2000-06-30"));
		
		assertEquals(21, cliente.getIdade());
	}
	
	@Test
	public void clienteCom5Anos() {
		CustomCalendar.setCurrentDate(LocalDate.parse("2020-02-15"));
		
		Cliente cliente = new Cliente();
		
		cliente.setDataNascimento(LocalDate.parse("2015-01-05"));
		
		assertEquals(5, cliente.getIdade());
	}
	
	@AfterEach
	public void afterEach() {
		CustomCalendar.useSystemDate();
	}
	
}
