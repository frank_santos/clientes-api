package dev.builders.clientes.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import dev.builders.clientes.entities.Cliente;
import dev.builders.clientes.exceptions.ValidationException;

public class ClienteValidationTest {
	
	ClienteService clienteService = new ClienteService();
	
	@Test
	public void dadosIncorretos() {
		
		assertValidation(() -> {
			clienteService.validar(null);			
		}, "Dados do cliente são obrigatórios");
		
		assertValidation(() -> {
			clienteService.validar(new Cliente());			
		}, "Nome é obrigatório");
		
		assertValidation(() -> {
			Cliente cliente = new Cliente();
			cliente.setNome(" ");
			clienteService.validar(cliente);			
		}, "Nome é obrigatório");
		
		assertValidation(() -> {
			Cliente cliente = new Cliente();
			cliente.setNome("FRANK SANTOS");
			cliente.setDataNascimento(LocalDate.parse("2000-01-01"));
			clienteService.validar(cliente);			
		}, "CPF é obrigatório");
		
		assertValidation(() -> {
			Cliente cliente = new Cliente();
			cliente.setNome("FRANK SANTOS");
			cliente.setCpf("abc123456");
			cliente.setDataNascimento(LocalDate.parse("2000-01-01"));
			clienteService.validar(cliente);			
		}, "CPF deve possuir 11 digitos numéricos");
		
		assertValidation(() -> {
			Cliente cliente = new Cliente();
			cliente.setNome("FRANK SANTOS");
			cliente.setCpf("12345678900");
			cliente.setDataNascimento(LocalDate.parse("2000-01-01"));
			clienteService.validar(cliente);			
		}, "CPF inválido");
		
		assertValidation(() -> {
			Cliente cliente = new Cliente();
			cliente.setNome("FRANK SANTOS");
			cliente.setCpf("65651227334");
			clienteService.validar(cliente);			
		}, "Data de Nascimento é obrigatória");
		
		assertValidation(() -> {
			Cliente cliente = new Cliente();
			cliente.setNome("FRANK SANTOS");
			cliente.setCpf("65651227334");
			cliente.setDataNascimento(LocalDate.parse("2030-01-01"));
			clienteService.validar(cliente);
		}, "Data de Nascimento inválida");
	}
	
	@Test
	public void dadosCorretos() {
		
		Cliente cliente = new Cliente();
		
		cliente.setNome("FRANK SANTOS");
		cliente.setCpf("65651227334");
		cliente.setDataNascimento(LocalDate.parse("1984-05-18"));
		
		clienteService.validar(cliente);		
	}
	
	private void assertValidation(Executable executable, String message) {
		ValidationException e = assertThrows(ValidationException.class, executable);
		assertEquals(message, e.getMessage());
	}
	
}
