package dev.builders.clientes.dtos;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import dev.builders.clientes.entities.Cliente;

public class ClienteDTOTest {
	
	@Test
	public void convertEntityToDTO() {
		Cliente entity = new Cliente();
		
		entity.setId(1L);
		entity.setNome("FRANK SANTOS");
		entity.setCpf("65651227334");
		entity.setDataNascimento(LocalDate.of(1984, 9, 20));
		
		ClienteDTO dto = ClienteDTO.of(entity);
		
		assertNotNull(dto);
		assertEquals(1L, dto.id);
		assertEquals("FRANK SANTOS", dto.nome);
		assertEquals("65651227334", dto.cpf);
		assertEquals(LocalDate.of(1984, 9, 20), dto.dataNascimento);
		
	}
	
	@Test
	public void convertDTOToEntity() {
		ClienteDTO dto = new ClienteDTO();
		
		dto.id = 2L;
		dto.nome = "FRANCISCO SANTOS";
		dto.cpf = "21580383300";
		dto.dataNascimento = LocalDate.of(1964, 3, 26);
		
		Cliente entity = dto.toEntity();
		
		assertNotNull(entity);
		assertEquals(2L, entity.getId());
		assertEquals("FRANCISCO SANTOS", entity.getNome());
		assertEquals("21580383300", entity.getCpf());
		assertEquals(LocalDate.of(1964, 3, 26), entity.getDataNascimento());
		
	}
	
}
