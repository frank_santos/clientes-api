package dev.builders.clientes.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class SortConverterTest {

	@Test
	public void oneField() {
		Sort sort = SortConverter.toSort("id");
		
		assertNotNull(sort);
		assertNotNull(sort.getOrderFor("id"));
		assertEquals(Direction.ASC, sort.getOrderFor("id").getDirection());
	}
	
	@Test
	public void oneFieldDesc() {
		Sort sort = SortConverter.toSort("id DESC");
		
		assertNotNull(sort);
		assertNotNull(sort.getOrderFor("id"));
		assertEquals(Direction.DESC, sort.getOrderFor("id").getDirection());
	}
	
	@Test
	public void twoFields() {
		Sort sort = SortConverter.toSort("id, nome");
		
		assertNotNull(sort);
		assertEquals(2, sort.toList().size());
		
		assertNotNull(sort.getOrderFor("id"));
		assertEquals(Direction.ASC, sort.getOrderFor("id").getDirection());
		
		assertNotNull(sort.getOrderFor("nome"));
		assertEquals(Direction.ASC, sort.getOrderFor("nome").getDirection());
	}
	
}
