package dev.builders.clientes.utils;

public class Assertions {
	
	public static void assertNotBlank(String actual) {
		assertNotBlank(actual, "expected: not <blank>");
	}
	
	public static void assertNotBlank(String actual, String message) {
		if (actual == null || actual.trim().isEmpty()) {
			org.junit.jupiter.api.Assertions.fail(message);
		}
	}
	
}
