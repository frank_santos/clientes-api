package dev.builders.clientes.controllers;

import static dev.builders.clientes.utils.Assertions.assertNotBlank;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.opentest4j.MultipleFailuresError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dev.builders.clientes.dtos.ClienteDTO;
import dev.builders.clientes.dtos.PageDTO;
import dev.builders.clientes.dtos.ResponseDTO;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class ClienteControllerTest {
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeEach
	public void setup() {
		restTemplate.getRestTemplate().getClientHttpRequestInitializers().add(request -> {
			request.getHeaders().add("Content-Type", "application/json");
		});
		restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
	}
	
	@Test
	@Order(1)
	public void getAll() throws Exception {
		ResponseEntity<String> response = restTemplate.getForEntity(getURL("/clientes"), String.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		PageDTO<ClienteDTO> pageDTO = objectMapper.readValue(response.getBody(), new TypeReference<PageDTO<ClienteDTO>>() {
		});
		
		assertNotNull(pageDTO);
		assertNotNull(pageDTO.content);
		assertEquals(20, pageDTO.content.size());
		assertEquals(0, pageDTO.pageNumber);
		assertEquals(20, pageDTO.contentSize);
		assertEquals(20, pageDTO.pageSize);
		assertEquals(50, pageDTO.total);
		assertEquals("id ASC", pageDTO.sort);
		
		assertPageContent(pageDTO);
	}

	private void assertPageContent(PageDTO<ClienteDTO> pageDTO) throws MultipleFailuresError {
		for (int i = 0; i < pageDTO.contentSize; i++) {
			ClienteDTO clienteDTO = pageDTO.content.get(i);
			
			assertAll("content[" + i + "]",
				() -> assertNotNull(clienteDTO),
				() -> assertNotNull(clienteDTO.id),
				() -> assertNotBlank(clienteDTO.nome),
				() -> assertNotBlank(clienteDTO.cpf),
				() -> assertNotNull(clienteDTO.dataNascimento)
			);
		}
	}
	
	@Test
	@Order(1)
	public void getAllByPage2() throws Exception {
		ResponseEntity<String> response = restTemplate.getForEntity(getURL("/clientes?pageNumber=2"), String.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		PageDTO<ClienteDTO> pageDTO = objectMapper.readValue(response.getBody(), new TypeReference<PageDTO<ClienteDTO>>() {
		});
		
		assertNotNull(pageDTO);
		assertNotNull(pageDTO.content);
		assertEquals(10, pageDTO.content.size());
		assertEquals(2, pageDTO.pageNumber);
		assertEquals(10, pageDTO.contentSize);
		assertEquals(20, pageDTO.pageSize);
		assertEquals(50, pageDTO.total);
		
		assertPageContent(pageDTO);
	}
	
	@Test
	@Order(1)
	public void getAllByCpf() throws Exception {
		String url = getURL("/clientes?cpf=98999103099");
		
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		PageDTO<ClienteDTO> pageDTO = objectMapper.readValue(response.getBody(), new TypeReference<PageDTO<ClienteDTO>>() {
		});
		
		assertNotNull(pageDTO);
		assertNotNull(pageDTO.content);
		assertEquals(1, pageDTO.content.size());
		assertEquals(0, pageDTO.pageNumber);
		assertEquals(1, pageDTO.contentSize);
		assertEquals(20, pageDTO.pageSize);
		assertEquals(1, pageDTO.total);
		
		assertPageContent(pageDTO);
	}
	
	@Test
	@Order(1)
	public void getAllByNome() throws Exception {
		String url = getURL("/clientes?nome=maria");
		
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		PageDTO<ClienteDTO> pageDTO = objectMapper.readValue(response.getBody(), new TypeReference<PageDTO<ClienteDTO>>() {
		});
		
		assertNotNull(pageDTO);
		assertNotNull(pageDTO.content);
		assertEquals(5, pageDTO.content.size());
		assertEquals(0, pageDTO.pageNumber);
		assertEquals(5, pageDTO.contentSize);
		assertEquals(20, pageDTO.pageSize);
		assertEquals(5, pageDTO.total);
		
		assertPageContent(pageDTO);
	}
	
	@Test
	@Order(1)
	public void getOne() throws Exception {
		String url = getURL("/clientes/1");
		
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		ClienteDTO clienteDTO = objectMapper.readValue(response.getBody(), ClienteDTO.class);
		
		assertNotNull(clienteDTO);
		assertEquals(1L, clienteDTO.id);
		assertEquals("ADAILTON XAVIER DE SOUSA", clienteDTO.nome);
		assertEquals("98999103099", clienteDTO.cpf);
		assertEquals(LocalDate.parse("1986-03-14"), clienteDTO.dataNascimento);
	}
	
	@Test
	@Order(3)
	public void put() throws IOException {
		ClienteDTO requestDTO = new ClienteDTO();
		
		requestDTO.cpf = "34815336830";
		requestDTO.nome = "ADELMA S. BASTOS";
		requestDTO.dataNascimento = LocalDate.parse("1993-08-20");
		
		URI url = URI.create(getURL("/clientes/3"));
		RequestCallback requestCallback = (request) -> { request.getBody().write(objectMapper.writeValueAsString(requestDTO).getBytes()); };
		ResponseExtractor<ClientHttpResponse> responseExtractor = clientHttpResponse -> clientHttpResponse;
		
		ClientHttpResponse response = restTemplate.execute(url, HttpMethod.PUT, requestCallback, responseExtractor);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		ClienteDTO clienteDTO = restTemplate.getForObject(getURL("/clientes/3"), ClienteDTO.class);
		
		assertNotNull(clienteDTO);
		assertEquals(3L, clienteDTO.id);
		assertEquals("ADELMA S. BASTOS", clienteDTO.nome);
		assertEquals("34815336830", clienteDTO.cpf);
		assertEquals("1993-08-20", clienteDTO.dataNascimento.toString());
	}
	
	@Test()
	@Order(4)
	public void patch() throws IOException {
		ClienteDTO requestDTO = new ClienteDTO();
		
		requestDTO.nome = "ADELCO LUIZ PEDROSO";
		
		URI url = URI.create(getURL("/clientes/2"));
		RequestCallback requestCallback = (request) -> { request.getBody().write(objectMapper.writeValueAsString(requestDTO).getBytes()); };
		ResponseExtractor<ClientHttpResponse> responseExtractor = clientHttpResponse -> clientHttpResponse;
		
		ClientHttpResponse response = restTemplate.execute(url, HttpMethod.PATCH, requestCallback, responseExtractor);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		ClienteDTO clienteDTO = restTemplate.getForObject(getURL("/clientes/2"), ClienteDTO.class);
		
		assertNotNull(clienteDTO);
		assertEquals(2L, clienteDTO.id);
		assertEquals("ADELCO LUIZ PEDROSO", clienteDTO.nome);
		assertEquals("00938854747", clienteDTO.cpf);
		assertEquals("1982-11-27", clienteDTO.dataNascimento.toString());
	}
	
	@Test
	@Order(5)
	public void delete() {
		restTemplate.delete(getURL("/clientes/50"));
		
		ResponseEntity<String> response = restTemplate.getForEntity(getURL("/clientes/50"), String.class);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test()
	@Order(6)
	public void create() {
		ClienteDTO requestDTO = new ClienteDTO();
		
		requestDTO.nome = "FRANK M M SANTOS";
		requestDTO.cpf = "65651227334";
		requestDTO.dataNascimento = LocalDate.parse("1984-09-20");
				
		ResponseEntity<ResponseDTO> response = restTemplate.postForEntity(getURL("/clientes"), requestDTO, ResponseDTO.class);
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals("Cliente registrado com sucesso", response.getBody().getMessage());
		assertNotNull(response.getBody().getAdditionalInfo().get("generatedId"));
		
		Long generatedId = Long.valueOf(response.getBody().getAdditionalInfo().get("generatedId").toString());
		
		ClienteDTO clienteDTO = restTemplate.getForObject(getURL("/clientes/" + generatedId), ClienteDTO.class);
		
		assertNotNull(clienteDTO);
		assertEquals(generatedId, clienteDTO.id);
		assertEquals("FRANK M M SANTOS", clienteDTO.nome);
		assertEquals("65651227334", clienteDTO.cpf);
		assertEquals("1984-09-20", clienteDTO.dataNascimento.toString());
	}
	
	@Test()
	@Order(6)
	public void createComCpfInvalido() {
		ClienteDTO requestDTO = new ClienteDTO();
		
		requestDTO.nome = "FRANCISCO C SANTOS";
		requestDTO.cpf = "21580383310";
		requestDTO.dataNascimento = LocalDate.parse("1964-03-23");
		
		ResponseEntity<ResponseDTO> response = restTemplate.postForEntity(getURL("/clientes"), requestDTO, ResponseDTO.class);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	private String getURL(String path) {
		return "http://localhost:" + port + path;
	}
	
}
